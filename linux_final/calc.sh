#!/usr/bin/env bash

# Warns if no arguments are supplied.
usage() {
  echo "No math problem file supplied!
Usage: $(basename $0) arg1 arg2" >&2
  exit 1
}
# Will execute all provided expressions if there are no two arguments
first() {
  while (( "$#" )); do
    echo $(cat $1)=$(($(cat $1)))
  shift
done
}
# Will execute if there are two arguments
second(){
  if [ $(($(cat $1))) -gt $(($(cat $2))) ]; then
    echo $(($(cat $1)))
  elif [ $(($(cat $1))) -eq $(($(cat $2))) ]; then
    echo "Expressions are equal: $(($(cat $1)))"
  else
    echo $(($(cat $2)))
  fi
}


if [ $# -eq 0 ]; then
  usage
elif [ $# -eq 2 ]; then
  second $1 $2
else
  first $*
fi
