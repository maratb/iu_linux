# Задание для промежуточной аттестации по модулю "Linux"

## Цель

Познакомить с программированием на bash:

параметрами командной строки

арифметической подстановкой

командной подстановкой

условиями

## Задача

Напишите простейший калькулятор:

Читает арифметическое выражение из файла переданного параметром и вычисляет его. В переданном файле одной строкой записано выражение, допустимо использовать круглые скобки.
Например, в файле input.txt записана строка (10+5)*(12/4). При запуске ./calc.sh input.txt на экран выведется результат вычисления 45

Доработайте калькулятор так, чтобы он производил вычисления для двух параметров и выводил тот результат, который больше

Опубликуйте полученный скрипт в git

Критерии оценки

Калькулятор на bash опубликован в git (github или gitlab)
Калькулятор может вычислить простейшие выражения типа (2+3)*5
Формат вызова: ./calc.sh file1 file2 (выражения для вычисления записываются в файлы)

## Решение:
### Конечный результат собранный из 2 скриптов содержится в ветке master. 
```
marat@Lenovo:~(master)$ ./calc.sh 
No math problem file supplied!
Usage: calc.sh arg1 arg2
marat@Lenovo:~(master)$ ./calc.sh file5 file4
25
marat@Lenovo:~(master)$ ./calc.sh file{1..5}
(10+5)*(12/4)=45
10%3=1
(10+5)*(12/2)=90
(10*3)-9=21
(2+3)*5=25
```

### 1 часть находится в ветке "first_task"
Применение.
```
marat@Lenovo:~/Documents/iu_linux/linux_final/first_task(first_task)$ ls
calc.sh  file1  file2  file3  file4  file5
marat@Lenovo:~/Documents/iu_linux/linux_final/first_task(first_task)$ ./calc.sh file{1..5}
Solution of the task from file1 is: (10+5)*(12/4)=45
Solution of the task from file2 is: 10%3=1
Solution of the task from file3 is: (10+5)*(12/2)=90
Solution of the task from file4 is: (10*3)-9=21
Solution of the task from file5 is: (2+3)*5=25
```
### 2 часть находится в ветке "second_task"
```
marat@Lenovo:~/Documents/iu_linux/linux_final/second_task(second_task)$ for i in $(seq 5); do
> echo "$(cat file$i)=$(($(cat file$i)))"
> echo
> done
(10+5)*(12/4)=45

10%3=1

(10+5)*(12/2)=90

(10*3)-9=21

(2+3)*5=25

marat@Lenovo:~/Documents/iu_linux/linux_final/second_task(second_task)$ ./calc.sh file1 file4
45
marat@Lenovo:~/Documents/iu_linux/linux_final/second_task(second_task)$ ./calc.sh file4 file5
25
marat@Lenovo:~/Documents/iu_linux/linux_final/second_task(second_task)$ ./calc.sh file3 file5
90

```
